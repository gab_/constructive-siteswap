# coding: utf-8


include SiteswapParser


class App < React::Component::Base

  export_state(
    :juggler,       :max_height,    :max_multi,
    :throw,         :siteswap,      :history,       :hist_index,
    :input_value,   :input_error,   :input_history, :input_index,
    :selected_hand, :selected_ball, :hover_hand,    :hover_ball
  )

  before_mount do
    siteswap = {async: "", sync: "", extended: ""}
    App.juggler!        Juggler.new(hands: 2, balls: 5)
    App.max_height!     9
    App.max_multi!      3
    App.throw!          []
    App.siteswap!       siteswap
    App.history!        []
    App.hist_index!     0
    App.input_value!    ""
    App.input_error!    ""
    App.input_history!  [""]   # thrown inputs
    App.input_index!    0
    App.selected_hand!  nil    # for ongoing throw setting
    App.selected_ball!  nil    # "
    App.hover_hand!     nil    # mouse detection hover hand labels
    App.hover_ball!     nil    # "                     balls
  end

  def render
    div.main_container {
      MenuBar()
      div.container {
        div.row {
          State()
          # TextState()
          InputBlock()
        }
      }
    }
  end

  def self.restart(args)
    siteswap = {async: "", sync: "", extended: ""}
    App.juggler!       Juggler.new(args)
    App.siteswap!      siteswap
    App.history!       [{}]
    App.hist_index!    0
    reset_throw
  end

  def self.save_history(i)
    App.history![i] = {
      :juggler       => App.juggler.dup,
      :siteswap      => App.siteswap.dup,
      :selected_hand => App.selected_hand,
      :input_value   => App.input_value,
      # :throw         => App.throw.map(&:dup)
    }
  end

  def self.charge_history(i)
    h = App.history[i]
    App.juggler!       h[:juggler].dup
    App.siteswap!      h[:siteswap].dup
    App.selected_hand! h[:selected_hand]
    App.input_value!   h[:input_value]
    # App.throw!         h[:throw].map(&:dup)
    parse_input
  end

  def self.move_input_history(k)
    i = App.input_index
    App.input_history![i] = App.input_value
    App.input_index! (i + {up: -1, down: 1}[k]) % App.input_history.size
    # App.input_index! [i + {up: -1, down: 1}[k], 0].min
    App.input_value! App.input_history[App.input_index]
    parse_input
  end

  # Empty throw, save input history
  def self.reset_throw
    `$('.input').focus();`
    App.throw! []
    App.selected_hand! nil
    input = App.input_value
    if App.input_history.last != input && !input.empty?
      App.input_history! << input
    end
    App.input_value! ""
    App.input_error! ""
    App.input_index! 0
  end

  def self.parse_input(implicit: true)
    # all undefined balls are staying in there hand (1x)
    default_height = 1
    to_hand = nil
    # throwing hand for asynchronous input (one hand, no comma)
    from_hand = App.juggler.throwing_hand
    puts "[parse_input] implicit: #{implicit}, from_hand: #{from_hand}, " \
      "input: #{App.input_value}"
    begin
      App.input_error! ""
      throw = SiteswapParser.parse_ss_throw(
        App.input_value,
        App.juggler.in_hands,
        default_height,
        from_hand,
        to_hand,
        implicit
      )
      throw.group_by{|path| path[:from_hand] }.each do |_, paths|
        paths.each_with_index do |path, i|
          path[:ball] = i
        end
      end
      App.throw!(throw) if implicit
      throw
    rescue SSParsingError => err
      App.input_error! err.to_s
      return false
    end
  end

  def self.throw_to_ss(throw, mode = :all, usage = :display)
    modes = (mode == :all ? [:async, :sync, :extended] : [mode])
    siteswap = {}
    modes.each do |mode|
      opts = case usage
      when :display
        non_breaking_space = {ruby: "\u00A0", html: "&nbsp;"}
        {
          :mode         => mode,
          :default_hand => App.juggler.current_hand,
          :hurry        => App.juggler.hurry,
          :spaces       => {hands: false, hurry: true} # non_breaking_space[:ruby]
        }
      when :input
        {
          :mode         => mode,
          :default_hand => App.juggler.from_hand,
          :spaces       => true
        }
      end
      siteswap[mode] = SiteswapParser.throw_to_ss(throw, App.juggler.in_hands, opts)
    end
    mode == :all ? siteswap : siteswap[mode]
  end

end



class RB < React::NativeLibrary
  imports 'RB'
end


class MenuBar < React::Component::Base
  
  def render
    # RB.Navbar.navbar_static_top {
    nav.navbar.navbar_default.navbar_static_top {
      div.container_fluid {
        div.navbar_header{
          div.navbar_brand{"Constructive Siteswap" }
          div.navbar_version{"alpha"}
        }
        Restart()
      }
    }
  end
end


class Restart < React::Component::Base

  before_mount do
    args = {balls: App.juggler.nb_ball, hands: App.juggler.nb_hand}
    # state.args! {}
    # args.each{|k, v| state.args![k] = v }
    args!(args)
  end

  def args
    {balls: state.balls, hands: state.hands}
  end

  def args!(h)
    h.each do |key, value|
      case key
      when :balls then state.balls! value.to_i
      when :hands then state.hands! value.to_i
      end
    end
  end

  def change(dir, key)
    value = args[key].to_i + {up: 1, down: -1}[dir]
    args!(key => value > 0 ? value : 0)
  end

  def render
    div.restart {
      # h4{ "Restart" }
      form.navbar_form.navbar_right {
        args.each do |key, value|
          RB.FormGroup {
            RB.ControlLabel{ key.capitalize }
            div.input_group {
              RB.FormControl(type: :text, value: args[key]).on(:change) { |e|
                args!(key => e.target.value)
                # state.error! ""
              }.on(:key_down) do |e|
                codes = {38 => :up, 40 => :down, 13 => :enter}
                case k = codes[e.key_code]
                when :up, :down then change(k, key)
                when :enter     then App.restart(args)
                end
              end
              div.input_group_btn {
                RB.Button{ RB.Glyphicon(glyph: 'plus') }.on(:click) do
                  args!(key => args[key].to_i + 1)
                end
                RB.Button(disabled: args[key] == 0){
                  RB.Glyphicon(glyph: 'minus')
                }.on(:click) do
                  value = args[key].to_i - 1
                  args!(key => value > 0 ? value : 0)
                end
              }
            }
          }
        end
        RB.Button{ "Restart" }.on(:click) do   # type: :submit
          App.restart(args)
        end
      }
    }
  end
end


class State < React::Component::Base

  def render
    puts "[State] App.juggler.states: #{App.juggler.states.inspect}"
    div.state.col_md_6 {
      RB.Table.center {
        # AIR
        tbody {
          max_height = App.max_height || App.juggler.states.map(&:size).max
          (max_height).downto(1) do |height|
            tr {
              th{ height.to_s }
              App.juggler.hands.each do |hand|
                td( cell_options(:air) ) {
                  render_balls(hand, height: height)
                }.on(:click){ add_path(height, hand) }
              end
            }
          end
        }
        # HAND
        tfoot {
          tr {
            th{ "0" }
            App.juggler.hands.each do |hand|
              td( cell_options(:balls, hand) ){
                render_balls(hand)
              }.on(:click){ select(hand) }.
                on(:mouse_enter){ App.hover_hand! hand }
            end
          }.on(:mouse_leave){ App.hover_hand! nil }
          tr {
            th
            App.juggler.hands.each do |hand|
              td( cell_options(:text, hand) ) {
                "Hand #{hand}"
              }.on(:click){ select(hand) }.
                on(:mouse_enter){ App.hover_hand! hand }
            end
          }.on(:mouse_leave){ App.hover_hand! nil }
          tr {
            th
            App.juggler.hands.each do |hand|
              td( cell_options(:set_throwing, hand) ) {
                RB.Glyphicon(glyph: 'triangle-top')
              }.on(:click){ set_throwing(hand) }
            end
          }
        }
      }
    }
  end

  def render_balls(hand, height: nil, per_line: 0)
    if height
      air_hand = :in_air
      balls_for = {
        :to_add     => 1,
        :from_throw => balls_from_throw(height, to_hand: hand),
        :present    => balls_present(hand, height: height)
      }
    else
      air_hand = :in_hand
      balls_for = {
        :to_throw   => balls_to_throw(hand),
        :present    => balls_present(hand)
      }
      # if hand != App.selected_hand && balls[:present].any?
      #   balls[:to_select] = [balls[:present].shift]
      # end
    end
    puts "[render_balls] hand: #{hand}, height: #{height} => #{balls_for}"
    cpt = 0
    [:to_add, :from_throw, :to_throw, :present].each do |type|
      b = balls_for[type] || 0
      balls = ( b.is_a?(Integer) ? (0...b).to_a : b )
      balls.each do |ball|
        br if cpt != 0 && per_line != 0 && cpt % per_line == 0

        render_ball(type, air_hand, hand, balls, ball)
        
        cpt += 1
      end
    end
    br if cpt.zero?
  end

  def render_ball(type, air_hand, hand, balls, ball)
    # RB.Glyphicon(options.merge(glyph: 'ball'))
    # span.circle(options){"—"}.on(:click) do
    #   case options[:class]
    #   # when /selected/        then   select(hand, ball)
    #   # when /present in-hand/ then   select(hand, ball)
    #   when /from-throw/      then reselect(hand, ball)
    #   end
    # end
    options = ball_options(type, air_hand, hand, balls, ball)
    if type == :from_throw && App.selected_hand.nil?
      span.circle.clickable(options){
        # div.glyphicon.glyphicon_remove
        div.cross1 ; div.cross2
      }.on(:click){ remove_path(ball) }.
        on(:mouse_enter){ App.hover_ball! ball }.
        on(:mouse_leave){ App.hover_ball! nil  }
    else
      div.circle(options)
    end
  end

  def balls_to_throw(hand)
    App.throw.select{|path| path[:from_hand] == hand }.map{|path| path[:ball] }
  end

  def balls_from_throw(height, to_hand: nil)
    paths = App.throw.select do |path|
      path[:height] == height &&
      ( to_hand.nil? || path[:to_hand] == to_hand )
    end
    # paths.map{|path| path[:ball] }
  end

  def balls_present(hand, height: nil)
    if height
      App.juggler.states[hand][height-1]
      # App.throw.select{|t| t[:to_hand] == hand && t[:height] == height }
    else
      (0 .. App.juggler.in_hands[hand] - 1).to_a - balls_to_throw(hand)
    end
  end

  def cell_options(type, hand = nil)
    c = type.to_s.tr('_', '-')
    case type
    when :air
      c += ' clickable' if App.selected_hand
      c += ' no-drop'   if App.selected_hand.nil?
      disabled = false   # !App.selected_hand
    when :set_throwing
      c += ' hand'
      c += ' clickable'
      c += ' current-hand'  if hand == App.juggler.current_hand
      c += ' throwing-hand' if hand == App.juggler.throwing_hand
      disabled = !!c['throwing-hand']
    when :balls, :text
      c += ' hand'
      c += ' clickable'
      c += ' hover'     if hand == App.hover_hand
      c += ' selected'  if hand == App.selected_hand
      c += ' to-cancel' if (path = App.hover_ball) && hand == path[:from_hand]
      # c += ' active'   if opts[:text] && ( c['selected'] || c[:hover] )
      disabled = balls_present(hand).empty?
    end
    {
      :class    => c,
      :disabled => disabled
    }
  end

  def ball_options(type, air_hand, hand, balls, ball)
    c = [:ball, type, air_hand]
    if    type == :present && air_hand == :in_hand
      c << :selected  if hand == App.selected_hand && ball == App.selected_ball
      c << :to_select if hand != App.selected_hand && ball == balls.first
    elsif type == :to_throw
      path = App.hover_ball
      c << :to_cancel if path && hand == path[:from_hand] && ball == balls.last
    end
    {
      :class => c.map{|c| c.to_s.tr('_', '-') }.join(' ')
    }
  end

  def deselect
    App.selected_hand! nil
    App.selected_ball! nil    
  end

  def select(hand, ball = nil)
    `$('.input').focus();`
    if App.selected_hand == hand && [App.selected_ball, nil].include?(ball)
      App.selected_hand! nil
      App.selected_ball! nil
    else
      present = balls_present(hand)
      ball ||= present.first
      if present.include? ball
        App.selected_hand! hand
        App.selected_ball! ball
      end
    end
  end

  def set_hover(hand)
    `$('td.hand').setState({hover: false})`
    # `$('td:nth-child(#{hand+2}).hand').addClass('hover')`
    `$('td:eq(#{hand}).hand').setState({hover: true})`
    `$('td:eq(#{hand}).hand.text').setState({hover: true})`
    # `$('td:nth-child(#{hand+2})').addClass('hover')`
  end

  def set_throwing(hand)
    `$('.input').focus();`
    App.juggler.throwing_hand!(hand)
    App.parse_input
  end

  def add_path(height, to_hand)
    return unless App.selected_hand
    `$('.input').focus();`
    App.throw! << {
      :height    => height,
      :from_hand => App.selected_hand,
      :ball      => App.selected_ball,
      :to_hand   => to_hand,
    }
    reset_input
  end

  def remove_path(path_or_heigth, from = nil, ball = nil, to = nil)
    `$('.input').focus();`
    if path_or_heigth.is_a? Hash
      path = path_or_heigth
    else
      path = {height: path_or_heigth, from_hand: from, ball: ball, to_hand: to}
    end
    App.throw!.delete_at App.throw.find_index(path)
    reset_input
    App.hover_ball! nil
  end

  # TODO: ?
  def remove_paths(height, hand)
    return if App.selected_hand
    `$('.input').focus();`
    App.throw! App.throw.reject do |path|
      path[:height] == height && path[:to_hand] == hand
    end
    reset_input
  end

  def reset_input
    i     = App.input_index
    value = App.input_value

    # 
    if i.zero? && value != state.last_value
      App.input_history! << value unless value.empty?
    else
      App.input_history![i] = value
      App.input_index! 0
    end
    App.input_value!  App.throw_to_ss(App.throw, :async, :input)
    state.last_value! App.input_value
    deselect
  end
end


class TextState < React::Component::Base

  def render
    hand_states  = App.juggler.states.each_with_index.map{|state, i|
      "Hand #{i}: #{state.join}" }
    to_throw     = App.juggler.in_hands.each_with_index.map{|in_hand, i|
      "Hand #{i}: #{in_hand}" }
    possib_vanil = possible_catches.each_with_index.map{|poss, i|
      "Hand #{i}: #{poss.join(', ')}" }
    possib_multi = possible_catches(:multi).each_with_index.map{|poss, i|
      "Hand #{i}: #{poss.join(', ')}" }
    div {
      "States: #{hand_states.join('   ')}".br
      "Balls to throw: #{to_throw.join('   ')}".br
      "Siteswap: #{App.siteswap.values.join('   ')}".br
      "Possible catches:".br
      # span{ "vanilia: " }
      # possible_catches.each_with_index.each do |poss, i|
      #   span{ "Hand #{i}: " }
      #   poss.each do |h|
      #     button(type: :button){ h.to_s }.on(:click) do
      #       Element['.input'].value = h
      #     end
      #   end
      # end
      # br
      "vanilia: #{possib_vanil.join('   ')}".br
      "multiplex: #{possib_multi.join('   ')}"
    }
  end

  # Returns possible catches by hand 
  def possible_catches(arg = nil)
    App.juggler.states.each_with_index.map do |state, i|
      (1..App.max_height).select do |h|
        t = state[h-1]
        if arg == :multi
          t > 0 && t < App.max_multi
        else
          t <= 0
        end
      end
    end
  end
end


class InputBlock < React::Component::Base
  def render
    div.input_block.col_md_6 {
      ThrowInput()
      History()
      Siteswap()
    }
  end
end


class ThrowInput < React::Component::Base

  after_mount do
    # Element['.input'].focus
    `$('.input').focus();`
  end

  def throw_input
    `$('.input').focus();`
    # return if App.input_value.empty? && !App.juggler.in_hands.all?(&:zero?)
    return unless throw = App.parse_input   # (implicit: false)
    puts "[throw_input] throw: #{throw.inspect} - " \
      "App.juggler.states: #{App.juggler.states.inspect}"
    siteswap = {}
    puts 1
    ss_throws = App.throw_to_ss(throw, :all, :display)
    puts 2
    ss_throws.each do |mode, ss_throw|
      siteswap[mode] = App.siteswap[mode] + ' ' + ss_throw
    end
    puts 3
    i = App.hist_index
    App.history! App.history[0..i] if App.history[i + 1]   # delete future
    App.save_history(i)
    puts 4
    App.hist_index! i + 1
    puts "[throw_input] Before throw - throw: #{throw.inspect}"
    App.juggler.throw!(throw)
    # App.juggler!  App.juggler
    puts "[throw_input] After throw - App.juggler.states: #{App.juggler.states.inspect}"
    App.siteswap! siteswap   # App.siteswap + ' ' + ss_throw
    App.reset_throw
  end

  def render
    opts = App.input_error.empty? ? {} : {validationState: :error}
    RB.FormGroup.throw_input.row(opts) {
      RB.Col(xs: 12){ div.form_line {
        div.has_feedback {
          RB.FormControl.input(
            type: :text,
            value: App.input_value,
            # placeholder: "Enter siteswap"

          ).on(:change) { |e|
            App.input_value! e.target.value
            App.parse_input

          }.on(:key_down) { |e|
            codes = {38 => :up, 40 => :down, 13 => :enter}
            case k = codes[e.key_code]
            when :up, :down
              App.move_input_history(k)
              # TODO: Doesn't work
              `$('.input')[0].setSelectionRange(0, #{App.input_value.length})`
            when :enter
              throw_input
            end

            # User can't use buttons with keyboard
            #   =>  `$('.input').focus();` everywhere!
            # }.on(:blur) { |e|
            #   `setTimeout(function(){ $('.input').focus(); }, 10)`
          }
          # RB::FormControl::Feedback()
          unless App.input_error.empty?
            span.form_control_feedback.glyphicon.glyphicon_remove
          end
        }
        submit = { id: :throw, bsStyle: :primary }   # type: :submit
        RB.Button{ "Reset" }.on(:click){ App.reset_throw }
        RB.Button(submit){ "Throw!" }.on(:click){ throw_input }
      }}
      RB.Col(xs: 12){ p.form_control_static.error{ App.input_error } }
    }
  end
end


class Siteswap < React::Component::Base
  def render
    # "Siteswap: #{App.siteswap}".br
    # RB.FormGroup do
    # RB.Table{ tbody{ tr{
    #   th{ "Siteswap" }
    #   td{ App.siteswap }
    # }}}
    div {
      # h4{ "Siteswap" }
      # RB.Well{ App.siteswap }
      h4{ "Siteswap" }
      div.siteswap.panel {
        App.siteswap.each do |mode, ss|
          div.list_group_item {
            ss.empty? ? br : ss
          }
        end
      }
      # h4{ "Siteswap" }
      # pre{ App.siteswap }
    }
  end
end


class History < React::Component::Base

  def render
    # RB.FormGroup do
    # RB.Table.history{ tbody{ tr{
    #   th{ "History" } td{
    div.history {
      h4{ "History" }
      RB.ButtonGroup {
        RB.Button(disabled: disabled?(:back)) {
          RB.Glyphicon(glyph: 'arrow-left')
        }.on(:click){ charge(:back) }
        RB.Button(disabled: disabled?(:forward)) {
          RB.Glyphicon(glyph: 'arrow-right')
        }.on(:click){ charge(:forward) }
      }
    }
  end

  def charge(dir)
    `$('.input').focus();`
    from = App.hist_index
    to   = from + {back: -1, forward: +1}[dir]
    raise("Can't go back before initial state!") if to < 0
    raise("No history record at index #{i}.") unless App.history[to]
    App.save_history from if ((s = App.history.size)-1..s).include? from
    App.charge_history to
    App.hist_index! to
  end

  def disabled?(dir)
    i = App.hist_index + {back: -1, forward: +1}[dir]
    i < 0 || App.history[i].nil?
  end
end



