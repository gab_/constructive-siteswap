require 'opal'
# require 'opal/compiler'   # no need to compile at runtime
require 'browser'
# require 'browser/socket'
# require 'browser/interval'
# require 'browser/delay'
# require 'opal-jquery'
# require 'react-latest'   # deprecated
# require 'reactrb'        # "

# Other documentation source
# require "opal"
# require "opal/version"
# require "browser"
# require "react"

# Doesn't work... Move to entry.js
# `var $ = require("jquery")`
# `var ReactDOM = require("react-dom")`
# `var React = require("react")`
# `require("style!css!./style.css")`

# require 'react/react-source'   # for React.js
require 'hyper-react'
require 'react/top_level_render'   # Fix warning
require 'reactrb/deep-compare'     # "

require './filled_array'
require './juggler'
require './siteswap_parser'
require './web_app'

# Error: No Element 
# Document.ready? do
#   Element['#app'].render{ App() }
# end

$document.ready do
  # React.render deprecated but ReactDOM.render doesn't work
  React.render(React.create_element(App), $document['#app'].to_n)
end
