

# require_relative './filled_array'   # crash opal-loader


class Juggler

  attr_reader :states, :in_hands, :nb_hand, :nb_ball, :current_hand, :hurry

  def initialize(hands: 2, balls: 3, current_hand: 0)
    @hurry   = 0
    @nb_hand = hands
    @nb_ball = balls
    per_hand, rest = @nb_ball.divmod(@nb_hand)
    
    @states   = Array.new(@nb_hand){ FilledArray.new(0) }
    @in_hands = Array.new(@nb_hand, per_hand)
    rest.times{|n| @in_hands[n] += 1 }
    @current_hand = current_hand
  end

  # Returns juggler's hands as interval of integers
  def hands
    0...@nb_hand
  end

  # Returns juggler's default throwing hand for next throw
  def from_hand
    (@current_hand + @hurry) % @nb_hand
  end
  alias :throwing_hand :from_hand

  # Returns juggler's default target hand for next throw
  def to_hand
    (from_hand + 1) % @nb_hand
  end
  alias :target_hand :to_hand

  # Set juggler's default throwing hand for next throw
  def from_hand!(h)
    hurry!(from_hand: h)
  end
  alias :throwing_hand! :from_hand!

  # Set juggler's hurry (offset of default throwing hand for next throw)
  def hurry!(offset = nil, from_hand: nil)
    [offset, from_hand].each do |a|
      raise ArgumentError if a && !a.is_a?(Integer)
    end
    if offset && from_hand
      raise ArgumentError if offset != (from_hand - @current_hand) % @nb_hand
    elsif from_hand
      offset = (from_hand - @current_hand) % @nb_hand
    end
    @hurry = offset || 1
  end

  # Throws the paths in the throw array and catches incoming balls
  def throw!(throw)
    throw.each{|p| throw_one!(p) }
    catch!
  end

  # Throws one ball from the path in argument
  def throw_one!(path = {})
    return if path[:height] <= 0
    @states[path[:to_hand]][path[:height] - 1] += 1
    @in_hands[path[:from_hand]] -= 1
  end

  # Catches every ball that falls at this time
  def catch!
    @states.each_with_index{|state, i| @in_hands[i] += state.shift }
    @current_hand = (@current_hand + @hurry + 1) % @nb_hand
    @hurry = 0
  end

  # Returns possible catches by hand 
  def possible_catches(arg = nil)
    @states.each_with_index.map do |state, i|
      (1..@max_height).select do |h|
        t = state[h-1]
        if arg == :multi
          t > 0 && t < @max_multi
        else
          t <= 0
        end
      end
    end
  end

  def dup
    copy = super
    @states = @states.map{|state| state.dup }
    @in_hands = @in_hands.dup
    copy
  end

end
