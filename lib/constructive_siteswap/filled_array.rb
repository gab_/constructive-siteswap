
# An array whith a default value
class FilledArray < Array
  
  attr_reader :default
  
  def initialize(default = nil, *args)
    @default = default
    super(*args)
  end

  def [](i)
    super(i) || @default
  end

  def shift
    super || @default
  end

  def join(*args)
    map{|n| n || @default }.join(*args)
  end
  
  def to_s
    map{|n| n || @default }.to_s
  end
  
end

