
module SiteswapParser

  NB_HAND = 2   # Default nb_hand
  ALNUMS = ['0'..'9', 'a'..'z', 'A'..'Z'].flat_map(&:to_a)
  SS_VANILIA_NO_SPACE_DIGIT_RE = /([0-9a-zA-Z])|\(\s*([0-9]+)\s*\)/
  SS_VANILIA_DIGIT_RE = /\s*(#{SS_VANILIA_NO_SPACE_DIGIT_RE.to_s})\s*/   # #to_s for Opal
  SS_DIGIT_RE = /\s*(#{SS_VANILIA_NO_SPACE_DIGIT_RE.to_s})(([xX]+)|\{\s*([0-9]+)\s*\})?\s*/


  ### Syntax ###

  # Accepting Gunswap syntax (www.gunswap.co/about/syntax)
  # TODO: Accepter P[2] dans la cas des ss de passing <|><|>
  HEIGHT  = /([0-9a-o])|\(\s*([0-9]+)\s*\)/
  TO_HAND = /\{\s*(\d*)\s*\}/
  MODIFIERS = {
    :to_hand => /([xX]+)(\{\s*(\d*)\s*\})?/,
    :pass    => /([pP]+)(\{\s*(\d*)\s*\})?/,   # /[pP](\{\s*([0-9]+)\s*\}|([0-9]+))?/
    :active  => /(A)/,
    :catch   => /(C)(\{\s*(C|P)?\s*\})?/,
    :throw   => /(T)(\{\s*(C|P)?\s*\})?/,
    :bounce  => /(B)(\{\s*(\d*)\s*(L|HL|F|HF)?\s*(\d*)\s*\})?/,
    :spin    => /(S)(\{\s*(-?\s*\d*(.\d*)?\s*(#{',\s*-?\s*\d*(.\d*)?\s*' * 3})?)?\s*\})?/,
    :dwell   => /(D)(\{\s*(\d*\.?\d*)?\s*\})?/,
  }
  
  TOSS      = /(#{HEIGHT.to_s})(#{TO_HAND.to_s})?(#{MODIFIERS.values.join('|')})*/
  MULTIPLEX = /\[(#{TOSS.to_s})+\]/
  THROW     = /#{TOSS.to_s}|#{MULTIPLEX.to_s}/
  SYNC      = /\((#{THROW.to_s}),(#{THROW.to_s})\)/
  BEAT      = /(#{THROW.to_s}|#{SYNC.to_s})/
  PASS      = /<#{BEAT.to_s}(\|#{BEAT.to_s})+>/
  PASS_COND = /<#{BEAT.to_s}+(\|#{BEAT.to_s}+)+>/ 
  SITESWAP  = /^(#{PASS.to_s})+|(#{BEAT.to_s})+\*?$/


  ### API ###

  # TODO: DOC
  def parse_ss_throw(ss_throw, in_hands,
        default_height = nil, from_hand = nil, to_hand = nil, implicit = false
      )
    ss_throw_to_throw(
      ss_throw, in_hands, default_height, from_hand, to_hand, implicit
    )
  end

  # Generate a siteswap string from a throw (array of paths)
  #
  # Options:
  # - default_hand: (default: 0)
  #     The hand that has to throw at this beat
  # - hurry: (default: 0)
  #     Shift of the `default_hand`
  #
  # Input:
  # - implicit_from_hand: (default: true)
  #     If not given, the throwing hand of a path is the `default_hand`
  # - implicit_to_hand: (default: true)
  #     If not given, the destination hand of a path is the next hand (modulo)
  # - implicit_default_paths: (default: true)
  #     Fill the throw with paths of `default_path_height`
  #     from remaining ball hands to themselves or to `default_path_to_hand`
  # - default_path_height: (default: 1)
  # - default_path_to_hand: (default: nil)
  #
  # Output:
  # - mode: (default: extended)
  #   :async (vanilia) =>
  #     Display only ss from `default_hand`
  #     All other balls must be 0 or `default_path_height` (if present)
  #   :sync =>
  #     Display ss from all hands but one beat in two (or it's a hurry)
  #   :extended =>
  #     Display ss from all hands at all beats
  # - beat: (default: :sync)
  #     Set if it's a synchronous beat (:sync or :async)
  # - space:
  #     This value for every `spaces`
  # - spaces: (default: {hands: '', hurry: ' ', async: ' '})
  #     Set spaces in ss (true => ' ')
  # - always_show_to_hand: (default: false)
  #     Print hand destination numbers instead of "x"s or nothing
  #     Implicitly, the destination hand is the next hand (modulo)
  # - show_default_paths: (default: true)
  #      Print paths of height 0 and default paths
  #
  def throw_to_ss(throw, in_hands, opts = {})
    throw_to_ss_throw(throw, in_hands, opts)
  end


  ### Options ###

  DEFAULTS = {
    :default_hand            => 0,
    :hurry                   => 0,

    :implicit_from_hand      => true,
    :implicit_to_hand        => true,
    :implicit_default_paths  => true,
    :default_path_height     => 1,
    :default_path_to_hand    => nil,
 
    :beat                    => :sync,
    :default_mode            => :extended,
    :mode                    => {
      :extended => {
        :sync                => true,
        :always_show_to_hand => true,
        :show_default_paths  => true,
      },
      :sync     => {
        :sync                => true,
        :always_show_to_hand => false,
        :show_default_paths  => false,
      },
      :async    => {
        :sync                => false,
        :always_show_to_hand => false,
        :show_default_paths  => false,
      }, 
    },
    :spaces => {hands: '', hurry: ' ', async: ' '},
  }

  private

  def defaults_merge(opts)
    opts = opts.dup unless opts[:bang]
    opts[:bang] = true

    # Consistency
    if [:sync, :async].all?{|k| opts.has_key? k }
      raise ArgumentError unless opts[:sync] != opts[:async]
    elsif opts.has_key? :async
      opts[:sync] = !opts[:async]
    end
    opts[:space]  = true         unless opts.has_key? :space
    opts[:spaces] = opts[:space] unless opts.has_key? :spaces
    [:async, :space].each{|k| opts.delete k }
    
    # Mode
    opts[:mode] ||= DEFAULTS[:mode]
    mode_syns = {
      :extended => [:ext,      :complete,     :all],
      :async    => [:asynchro, :asynchronous, :vanilia, :simple],
      :sync     => [:synchro,  :synchronous,  :all_hands],
    }
    mode_syns.each{|m, s| opts[:mode] = m if s.include? opts[:mode] }
    unless mode_syns.keys.include? opts[:mode]
      raise(ArgumentError, "Mode is unknown")
    end

    # Merge
    opts[:spaces] = set_spaces(opts[:spaces], DEFAULTS[:spaces])
    defaults = DEFAULTS.merge(DEFAULTS[:mode][opts[:mode]])
    defaults.each{|k, v| opts[k] = v unless opts.has_key? k }
    opts
  end

  def set_spaces(spaces, defaults)
    result = {}
    defaults.each do |type, d|
      result[type] = case spaces
        when Hash   then s = spaces[type] ; s.is_a?(String) ? s : (s ? d : '')
        when String then spaces
        else spaces ? d : ''
      end
    end
    result
  end


  ### Errors ###

  class SSParsingError    < RuntimeError; end
  class InvalidThrowError < RuntimeError; end
  SE = SSParsingError
  TE = InvalidThrowError


  ### Throw -> SS ###

  # Returns the siteswap height corresponding to the integer height.
  def height_to_ss_height(height)
    err_bad_height = "The height #{height} must be an integer."
    raise(TE, err_bad_height) unless height.is_a?(Integer)

    if height < ALNUMS.size then ALNUMS[height]
    else '(' + height.to_s + ')'
    end
  end

  # Returns the siteswap digit corresponding to the path (height, throwing hand, target hand)
  def path_to_ss_digit(height, from_hand, to_hand, nb_hand, opts = {})
    err_bad_from = "The throwing hand #{from_hand} does not exist."
    err_bad_to   = "The target hand #{to_hand} does not exist."
    raise(TE, err_bad_from) if from_hand && (from_hand < 0 || from_hand >= nb_hand)
    raise(TE, err_bad_to  ) if to_hand   && (to_hand   < 0 || to_hand   >= nb_hand)
    opts = defaults_merge(opts)
    
    default_to_hand = from_hand + (height % nb_hand)
    to_hand ||= default_to_hand if opts[:implicit_to_hand]
    dist = (to_hand - default_to_hand) % nb_hand
    implicit = !opts[:always_show_to_hand]
    x = if  to_hand == default_to_hand && (nb_hand <= 2 || implicit)
      ''
    elsif nb_hand <= 2 || (implicit && dist <= 3)
      'x' * dist
    else
      '{' + to_hand.to_s + '}'
    end
    height_to_ss_height(height) + x
  end

  # Returns an array of siteswap digits corresponding to the paths of each hand
  def throw_to_hands_ss_digits(throw, in_hands, opts = {})
    opts = defaults_merge(opts)
    nb_hand = in_hands.size

    in_hands.each_with_index.map do |in_hand, hand|
      hand_paths = throw.select{|p| p[:height] != 0 && p[:from_hand] == hand }
      err_nb_ball = "The hand #{hand} has #{in_hand} balls and can not " \
        "throw #{hand_paths.size}."
      raise(TE, err_nb_ball) unless in_hand == hand_paths.size
      ss_digits = []
      hand_paths.sort{|p1, p2| p2[:height] <=> p1[:height] }.each do |p|
        # sort_by{|p| p[:height] }.reverse
        if !p[:default_path] || opts[:show_default_paths]
          ss_digits <<
            path_to_ss_digit(p[:height], hand, p[:to_hand], nb_hand, opts)
        end
      end
      case ss_digits.size
      when 0
        if opts[:show_default_paths] ||
           (opts[:sync] && hand == opts[:default_hand] && in_hands.all?(&:zero?))
        then 0
        else ''
        end
        # if async_mode && hand == opts[:default_hand] && in_hands.all?(&:zero?)
        #   if in_hand.zero? then '0'
        #   else
        #     path_to_ss_digit(
        #       opts[:default_path_height],
        #       hand, opts[:default_path_to_hand] || hand, nb_hand, opts)
        #   end
        # elsif opts[:show_default_paths] then '0'
        # else ''
        # end
      when 1 then ss_digits.first
      else '[' + ss_digits.join + ']'
      end
    end  
  end

  # Fill paths with default throwing hand and the throw with default path
  def fill_throw(throw, in_hands, opts = {})
    err_bad_thow = "The throw must be an array off paths (hash)" \
      "with at least an :heigth"
    raise(TE, err_bad_thow) unless throw.is_a? Array
    opts = defaults_merge(opts)
    throw = throw.dup

    throw.each do |path|
      raise(TE, err_bad_thow) unless path.is_a?(Hash) && path.has_key?(:height)
      if opts[:implicit_from_hand]
        path[:from_hand] ||= opts[:default_hand]
      end
    end

    if opts[:implicit_default_paths]
      in_hands.each_with_index.map do |in_hand, hand|
        hand_paths = throw.select{|p| p[:from_hand] == hand && p[:height] != 0 }
        (in_hand - hand_paths.size).times do
          throw << {
            :height       => opts[:default_path_height],
            :from_hand    => hand,
            :to_hand      => opts[:default_path_to_hand] || hand,
            :default_path => true
          }
        end
      end
    end

    throw
  end

  # Returns the siteswap throw corresponding to the array of paths
  def throw_to_ss_throw(throw, in_hands, opts = {})
    opts = defaults_merge(opts)
    spaces = opts[:spaces]
    nb_hand = in_hands.size
    async_mode = !(sync_mode = opts[:sync])

    throw = fill_throw(throw, in_hands, opts)
    ss_hands = throw_to_hands_ss_digits(throw, in_hands, opts)

    # Output options derived from ss_hands
    not_emptys = ss_hands.reject{|e| e == '' }
    only_hand  = ss_hands.find_index(one = not_emptys.first) if not_emptys.one?
    from_hand  = (opts[:default_hand] + opts[:hurry]) % nb_hand
    sync_beat  = opts[:beat] == :sync
    bang =     !sync_mode || sync_beat ? '' : ('!' + spaces[:async])
    star = if !async_mode || from_hand == opts[:default_hand]
      ''
    elsif opts[:hurry] <= 3
      '*' * opts[:hurry] + spaces[:hurry]
    else
      '*{' + from_hand.to_s + '}' + spaces[:hurry]
    end

    ss_throw = if nb_hand.zero?
      '0'
    elsif async_mode && not_emptys.empty?
      '0'
    # elsif async_mode && default_paths && not_emptys.empty?
    #   if in_hands[from_hand] > 0
    #     path_to_ss_digit(
    #       opts[:default_path_height] || 1, from_hand, default_to_hand || from_hand,
    #       nb_hand, opts
    #     )
    #   else '0'
    #   end
    elsif async_mode && not_emptys.one? && from_hand == only_hand
      one
    elsif sync_mode && !sync_beat && not_emptys.empty?
      ''
    else
      bang + '(' + ss_hands.join(',' + spaces[:hands]) + ')'
    end
    star + ss_throw
  end


  ### SS -> Throw ###

  # Returns the path (height, throwing hand, target hand) corresponding to the siteswap digit ss_d
  def ss_digit_to_path(ss_d, from_hand, nb_hand = NB_HAND, match = nil)
    ss_d = ss_d.strip
    err_bad_from  = "The throwing hand #{from_hand} does not exist."
    err_no_height = "Can't parse siteswap digit \"#{ss_d}\"."
    err_bad_x     = "The target hand modifier \"x\" in siteswap digit \"#{ss_d}\" is only for two hands patterns."
    err_no_to     = "Can't parse target hand in siteswap digit \"#{ss_d}\"."
    # err_bad_to    = "The target hand %s from siteswap digit \"#{ss_d}\" does not exist."
    raise(SE, err_bad_from) if from_hand < 0 || from_hand >= nb_hand

    _, _, alnum, num, _, x, n_hand = match || ss_d.match(/^#{SS_DIGIT_RE.to_s}$/).to_a
    height = if alnum   then ALNUMS.index(alnum)
      elsif num         then num.to_i
      elsif ss_d.empty? then 0
    end
    raise(SE, err_no_height) unless height
    # raise(SE, err_bad_x) if x && nb_hand != 2
    # cross = (nb_hand == 2 && height.odd?) ? 1 : 0
    # cross = height.odd? ? 1 : 0
    cross = height % nb_hand
    # next_hand = lambda{|n| (from_hand + n + cross) % nb_hand }
    # to_hand = if x then next_hand.(x.size)
    #   elsif n_hand then n_hand.to_i
    #   else              next_hand.(0)
    # end
    next_hand = (from_hand + (x ? x.size : 0) + cross) % nb_hand
    to_hand = n_hand ? n_hand.to_i : next_hand
    raise(SE, err_no_to) unless to_hand
    raise(SE, err_bad_to % to_hand) if to_hand >= nb_hand
    {height: height, from_hand: from_hand, to_hand: to_hand}
  end

  # Returns an array of paths corresponding to the vanilia siteswap ss_vanilia
  def ss_vanilia_to_throw(ss_vanilia, from_hand, nb_hand = NB_HAND, multi = false)
    err_syntax = "The syntax of the vanilia siteswap \"#{ss_vanilia.strip}\" is not correct."
    raise(SE, err_syntax) unless ss_vanilia =~ /^(#{SS_DIGIT_RE.to_s})*$/
    ss_vanilia.scan(/(#{SS_DIGIT_RE.to_s})/).each_with_index.map do |match, i|
      from = multi ? from_hand : (from_hand + i) % nb_hand
      ss_digit_to_path(match.first, from, nb_hand, match)
    end
  end

  # Returns an array of paths corresponding to the asynchronous siteswap ss_async
  # Optionaly fill the throw with paths of height `default_height`
  # from other hands to themselves or `to_hand` hand
  def ss_async_to_throw(ss_async, from_hand, in_hand,
        nb_hand = NB_HAND, default_height = nil, to_hand = nil, implicit = false
      )
    ss = ss_async.to_s.strip
    err_nb_ball = "The hand #{from_hand} has #{in_hand} balls and can not throw %s."
    if ss[0] == '[' && ss[-1] == ']'
      ps = ss_vanilia_to_throw(ss[1..-2], from_hand, nb_hand, :multi)
    else
      ps = [ss_digit_to_path(ss, from_hand, nb_hand)]
    end
    ps.reject!{|p| p[:height].zero? }
    rest = in_hand - ps.count
    if !implicit && default_height && default_height > 0 && rest > 0
      path = {height: default_height, from_hand: from_hand, to_hand: to_hand || from_hand}
      ps += [path] * rest
    end
    if implicit ? ps.size > in_hand : ps.size != in_hand
      raise(SE, err_nb_ball % ps.size)
    end
    ps
  end

  # Returns an array of paths corresponding to the siteswap throw ss_throw
  # Optionaly fill the throw with paths of height `default_height`
  # from other hands to themselves or `to_hand` hand
  # Accept asynchronous siteswap from `from_hand` or only hand with balls if any
  def ss_throw_to_throw(ss_throw, in_hands,
        default_height = nil, from_hand = nil, to_hand = nil, implicit = false
      )
    ss = ss_throw.to_s.strip
    nb_hand = in_hands.length
    err_bad_nb_hand = "The siteswap throw \"#{ss}\" is not for #{nb_hand} hands."
    err_bad_from    = "The throwing hand #{from_hand} does not exist."
    err_bad_to      = "The target hand #{to_hand} does not exist."
    err_bad_height  = "The default height for other hands must be an integer."
    raise(SE, err_bad_from) if from_hand && (from_hand < 0 || from_hand >= nb_hand)
    raise(SE, err_bad_to)   if to_hand   && (to_hand   < 0 || to_hand   >= nb_hand)
    raise(SE, err_bad_height) if default_height && !default_height.is_a?(Integer)
    
    if ss[',']
      ss = ss[1..-2] if ss[0] == '(' && ss[-1] == ')'
      parts = ss.split(',', -1)
      raise(SE, err_bad_nb_hand) if parts.size != nb_hand
    else
      parts = nb_hand.times.map{|hand| ss if hand == from_hand }
    end
    in_hands.each_with_index.map { |in_hand, hand|
      ss_async_to_throw(parts[hand], hand, in_hand, nb_hand, default_height, to_hand, implicit)
    }.flatten
  end

end
